# [EscapeRoom](https://cyberdefenders.org/labs/18)

#### Scenario
> You belong to a company specializing in hosting web applications through KVM-based Virtual Machines. Over the weekend, one VM went down, and the site administrators fear this might be the result of malicious activity. They extracted a few logs from the environment in hopes that you might be able to determine what happened.

> This challenge is a combination of several entry to intermediate-level tasks of increasing difficulty focusing on authentication, information hiding, and cryptography. Participants will benefit from entry-level knowledge in these fields, as well as knowledge of general Linux operations, kernel modules, a scripting language, and reverse engineering. Not everything may be as it seems. Innocuous files may turn out to be malicious so take precautions when dealing with any files from this challenge.

#### Tools Used
* Wireshark
* John the Ripper
* UPX

---
1. **What service did the attacker use to gain access to the system?**
> `ssh`
> First lot of packets are mostly *SSH*.

2. **What attack type was used to gain access to the system?(one word)**
> `bruteforce`
> Just by looking at the number of *SSH* sessions being spawned in such a short amount of time, we can see that the attacker is trying to brute force their way in.

3. **What was the tool the attacker possibly used to perform this attack?**
> `hydra`
> This is a common tool used for brute forcing entry onto a system.

4. **How many failed attempts were there?**
> `52`
> After applying the filter `ssh`, I opened up the *Conversations* view from the *Statistics* menu, and set it to limit using the applied filter in the *TCP* tab. This gave the number **54** at the top.

5. **What credentials (username:password) were used to gain access? Refer to shadow.log and sudoers.log.**
> `manager:forgot`
> Username in the hint is 7 characters long, looking at the **shadow.log** file, there are a number of users with that length; *timothy* and *manager*.
> `john --wordlist=rockyou.txt --users=manager shadow.log` = **forgot**
> This produced the password almost instantaneously.

6. **What other credentials (username:password) could have been used to gain access also have SUDO privileges? Refer to shadow.log and sudoers.log.**
> `sean:spectre`
> The hint shows a 4 letter username. The only one which meets that criteria other than *root* is *sean*.
> `john --wordlist=rockyou.txt --users=sean shadow.log` = **spectre**

7. **What is the tool used to download malicious files on the system?**
> `wget`
> Starters was a lucky guess based on the length of the hint in the text input. But this is confirmed with this user agent for one of the files which was downloaded:
> ![07 - User Agent](18-User_Agent.png)

8. **How many files the attacker download to perform malware installation?**
> `3`
> This was yet another guess based on the hint in the text input. But this was confirmed when answering the next question. The files `1` and `2` are both **ELF** executables, and `3` is a BASH script.

9. **What is the main malware MD5 hash?**
> `772b620736b760c1d736b1e6ba2f885b`
> Gained by running `md5sum` on `1`.

10. **What file has the script modified so the malware will start upon reboot?**
> `/etc/rc.local`
> Found when looking at the BASH script.

11. **Where did the malware keep local files?**
> `/var/mail/`
> Like the previous question, this is found when examining the BASH script.

12. **What is missing from ps.log?**
> `/var/mail/mail`
> When examining the *ps.log* file, this process is missing from the list.

13. **What is the main file that used to remove this information from ps.log?**
> `sysmod.ko`
> When looking at this file under **ida**, there is a function called `remove_proc_entry`

14. **Inside the Main function, what is the function that causes requests to those servers?**
> `requestFile`
> Looking at `1` in **ida**, there isn't much to go on with function names like there is with `2`. The executable has compressed using **UPX**, running `upx -d 1` decompresses it. When opening it back up in **ida**, the functions names are a little clearer.

15. **One of the IP's the malware contacted starts with 17. Provide the full IP.**
> `174.129.57.253`
> With the hint in the question, the IP address can be found in the *Conversations* view. The host can be seen communicating with the server in question.

16. **How many files the malware requested from external servers?**
> `9`
> Looking at the list of files in the *HTTP Exports Objects*, there are a number of files which have what appears to be **Base64** encoded filenames.

17. **What are the commands that the malware was receiving from attacker servers? Format: comma-separated in alphabetical order**
> 
